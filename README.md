# Pad2print

Pad2Print est un outil de mise en page de documents imprimés collaboratif dans le navigateur web. Il utilise des pads sur lesquels il est possible d'éditer le css et permet une prévisualisation en temps réel de la double-page.  
Ne fonctionne que sur Chrome ou Chromium.  
Cet outil est en cours de réécriture et fera l'objet d'une réelle documentation bientôt.

## Screenshots

![image](exemple/1.png)
![image](exemple/2.png)
![image](exemple/3.png)

## Démo

[http://www.luuse.io/pad2print_demo/](http://www.luuse.io/pad2print_demo/)

## todo
- setup: définir un format
- setup: générer une grille
- si nouvelle instance, nouveau pad documentation
- si nouvelle instance, formulaire pour nom de projet
- retirer les noms de personnes
- ~~toggle grille~~
- upload images
- gestion de la double-page (si le contenu s'étale sur les deux)
- afficher setup dans l'interface
- doc passer avec une seule variable (pas une pour js et une pour php)
- du ménage!!
- ...
