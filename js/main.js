$(document).ready(function(){

  var iframeDoc     = 'https://semestriel.framapad.org/p/demo_doc';
  var reload        = $('#nav').children('.reload');
  var padsToggle    = $('#nav').children('.pads');
  var gridToggle    = $('#nav').children('.grid');
  var padsParts     = $('#nav').children('.pad');
  var sizer         = $('#nav').children('.sizer');
  var edit_doc      = $('#doc').children('#edit_doc');

  var pads          = $('#pads');
  var double        = $('.double');
  var doubleBack    = $('.doubleBack');
  var pageGHome     = double.children('#pageGauche').children('.gauche');
  var pageDHome     = double.children('#pageDroite').children('.droite');
  var grid          = $('.theGrid');
  var pageG         = $('#pageGauche.single').children('.pages');
  var pageD         = $('#pageDroite.single').children('.pages');
  var gridG         = $('#pageGauche.single').children('.theGrid');
  var gridD         = $('#pageDroite.single').children('.theGrid');
  var page          = $('#pages');
  var docPad        = $('#doc').children('iframe');

  reloadPreview(reload, pageG, pageD);
  showPadsAndGrid(padsToggle, pads, false);
  showPadsAndGrid(gridToggle, grid, false);
  showPadContent(padsParts, pads);
  // showPads(edit_doc, docPad, true);
  zoom(sizer, pageG, pageD, gridG, gridD);
  zoomHome(sizer, double, pageGHome, pageDHome);


  edit_doc.click(function(){

    if($('#cont_doc').children('iframe').attr('id') == 'iframe_doc'){
      $('#cont_doc').load('include/doc.php');
      $(this).html('Éditer');
    } else {
      $('#cont_doc').html($('<iframe/>', {
      	id: 'iframe_doc',
      	src: iframeDoc,
      }));
      $(this).html('Retour');
    }
  });


});
